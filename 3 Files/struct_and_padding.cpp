#include <iostream>

using namespace std;

/* Empaquetar la estructura en una sola columna al que hace referencia el 1,
de tal manera que consuma menos memoria.*/

#pragma pack(push, 1)
//#pragma pack(pop);
struct Person {
    //string name;
    char name[50];
    int age;
    double weight;
};

// Desempaqueta la estructura.
#pragma pack (pop)

int main () {

    cout << sizeof(Person) << endl;

    return 0;
}
