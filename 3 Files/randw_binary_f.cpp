#include <iostream>
#include <fstream>

using namespace std;

#pragma pack(push, 1)

struct Person {
       char name [50];
       int age;
       double weight;
};

#pragma pack (pop)

int main () {

    Person someone {"Frodo", 220, 0.8};
    string fileName = "test.bin";
    ofstream outputFile;
    outputFile.open(fileName, ios::binary);

    /*fstream outputFile
     * outputFile.open(fileName, ios::binary | ios::out);
    */

    /* Read Binary File */

    if(outputFile.is_open()) {
        outputFile.write((char*)&someone, sizeof(Person));
        outputFile.close();
    }
    else {
        cout << "No se crea el archivo" + fileName;
    }

    return 0;
}
