#include "Complex.h"


namespace  caveofprogramming {
	
        ostream &operator<<(ostream &out, const Complex &c) {
            out << "(" << c.getReal() << "," << c.getImaginary() << ")";
            return out;
        }

	Complex::Complex() {}

	Complex::Complex(double real, double imaginary) : real(real), imaginary(imaginary)  {}

	Complex::Complex(const Complex &other) {
		cout << "Copy" << endl;
		real = other.real;
		imaginary = other.imaginary;
	}

        const Complex &Complex::operator = (const Complex &other){
            real = other.real;
            imaginary = other.imaginary;

            return *this;

        }
}
