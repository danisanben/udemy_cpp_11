#include <iostream>
#include <map>

using namespace std;

int main (){
    map<string, int> ages;

    ages["Mike"] = 40;
    ages["Raj"] = 20;
    ages["Vicky"] = 30;

    ages["Mike"] = 70;

    cout << ages ["Raj"] << endl;
    cout << ages ["Vicky"] <<endl;

    cout << ages ["Sue"] <<endl;

    for(map<string, int>::iterator it=ages.begin(); it!=ages.end(); it++){
        cout<< it->first << ": " << it->second << endl;
    }

    return 0;
}
