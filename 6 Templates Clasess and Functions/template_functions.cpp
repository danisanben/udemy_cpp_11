#include <iostream>
using namespace std;

template <class T>
void print(T n){
    cout << n << endl;
};

int main () {
    print<string>("Hello 5");
    print<int>(20);
    print<float>(10.052);
    return 0;
}