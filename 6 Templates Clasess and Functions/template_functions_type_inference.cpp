#include <iostream>

using namespace std;

template <class T>
void print(T n) {
    cout << "Template version: " << n << endl;
}

void print(int value) {
    cout << "Non template version: " << value << endl;
}

template <class T>
void show () {
    cout << T() << endl;
}

int main () {
    print<string>("Hello");
    print<int>(5);
    print<int>(5.12);

    print(6);
    print<>(7.25);

    show <double> ();

    return 0;
}
