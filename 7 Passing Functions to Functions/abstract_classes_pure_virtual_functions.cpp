#include <iostream>

using namespace std;

class Animal {
public:
    virtual void speak() = 0;
    virtual void run() = 0;
};

class Dog: public Animal{
public:
    virtual void speak() {
        cout << "Moof" << endl;
    }
};

class Labrador: public Dog{
public:
    Labrador(){
        cout << "new labrador" << endl;
    }
    virtual void run() {
        cout << "Labrador corriendo" << endl;
    }
};

void test (Animal &a){
    a.run();
}

int main (){

    Labrador labs[5];

    Labrador lab;
    lab.speak();
    lab.run();

    Animal *animals[5];
    animals[0] = &lab;
    animals[1] = &lab;

    animals[0]->speak();
    animals[1]->run();

    return 0;
}