#include <iostream>
#include <memory>

using namespace std;

class Test{
public:
    Test(){
        cout << "created" << endl;
    }

    void greet(){
        cout << "Hello" << endl;
    }

    ~Test(){
        cout << "destroyed" << endl;
    }
};


int main ()
{
//    cout << "Hello World!!!!!!!!!!!" << endl; //imprime Hello!!!!!

//    Test test;

    shared_ptr<Test> pTest(new Test());

    cout << "Terminado" << endl;

    return 0;
}
