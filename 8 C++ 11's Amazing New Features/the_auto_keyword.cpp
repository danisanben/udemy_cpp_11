#include <iostream>
#include <typeinfo>

using namespace std;

template <class T, class S>
auto test(T value, S value_2) -> decltype (value + value_2){
    return value + value_2;
}

auto get(){
    return 999;
}

auto test2 () -> decltype(get()){
    return get();
}

int main (){
    auto value = 39;
    auto value_2 = "hola que tal";


    cout << test(5 , 6)<< endl;
    cout << test2 () << endl;

    return 0;

}