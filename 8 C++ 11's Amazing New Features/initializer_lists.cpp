#include <iostream>
#include <vector>

using namespace std;

class Test{
  public:
    Test(initializer_list<string> texts){
        for (auto value: texts){
            cout << value << endl;
        }
    }
    void print(initializer_list<string> texts){
        for (auto value: texts){
            cout << value << endl;
        }

    }
};

int main ()
{
    vector<int>numbers{1,2,3,4,7};
    cout << numbers[2]<< endl;

    Test test{"apple", "orange", "banana"};
    cout << endl;
    test.print({"one", "two", "three", "four"});

    return 0;
}
