#include <iostream>

using namespace std;

void test(void (*pFunc)()){
    pFunc();
}

int main (){

    auto func = [](){cout << "Hola" << endl;};

    func();

    [](){cout << "Hallo" << endl;}();

    test(func);

    test([](){cout << "Hola" << endl;});

    return 0;

}