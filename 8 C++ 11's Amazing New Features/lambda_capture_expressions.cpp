#include <iostream>

using namespace std;

int main (){

	int one=1;
	int two=2;
	int three=3;

	[one, two](){cout << one << ", " << two << endl;}();

	[=](){cout << one << ", " << two << endl;}();

	[=, &three](){three=7; cout << one << ", " << two << endl;}();
	cout << three << endl;

	[&](){three =7; two=8; cout << one << ", " << two << endl;}();
	cout << two << endl;

	[&, two, three](){one =100; cout << one << ", " << two << endl;}();
	cout << one << endl;



	return 0;
}