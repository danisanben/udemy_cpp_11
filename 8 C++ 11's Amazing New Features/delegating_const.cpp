#include <iostream> 

using namespace std;

class Parent {
	int dogs;
	string text;

public:
	Parent(): Parent("hello"){

	dogs = 5;
	cout << "No parameter parent cosntructor " << endl;
}
	Parent (string text) {
	dogs = 5;
	this -> text = text;
	cout << "string parent constructor" << endl;
	}
};

class Child: public Parent {
public:
	//Child() {

//}
	Child() = default;
};

int main () {
	Parent parent ("Hello");
	Child child;
	return 0;
}
