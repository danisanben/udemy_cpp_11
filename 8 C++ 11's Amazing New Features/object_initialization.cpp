#include <iostream>

using namespace std;

class Test {
    int id{3};
    string name{"Dani"};

public:
    Test()=default;
    Test(const Test &other) = default;
    Test &operator=(const Test &other) = default;

    Test(int id): id(id){}
    Test(string name): name(name){}

    void print(){
        cout << id << ": " << name << endl;
    }
};

int main (){

    Test tests;
    tests.print();

    Test tests1 (25);
    tests1.print();

    Test tests2("Manue");
    tests2.print();

    return 0;

}