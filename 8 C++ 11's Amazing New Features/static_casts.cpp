#include <iostream>

using namespace std;

class Parent
{
public:
    void speak() {
        cout << "parent!" << endl;
    }

};

class Brother: public Parent
{

};

class Sister: public Parent
{

};

int main ()
{
    float value = 3.23;
    Parent parent;
    Brother brother;


    Parent *ppb = &brother;

    Brother *pbb = static_cast<Brother *>(ppb);

//    Brother *pbb = &brother;

//    Parent *pp = &brother;

//    cout << int (value) << endl;

//    Brother *pb = static_cast <Brother *>(&parent);

//    cout << pb << endl;

//    Parent &&p = Parent ();

    Parent  &&p = static_cast<Parent &&>(parent);

    p.speak();

    return 0;

}